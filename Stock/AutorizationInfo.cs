﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Windows;
namespace Stock
{
    class user
    {
        public string username;
        public string password;
    }
    public class Auth
    {
        public string statusCode;
        public string message;
        public Dictionary<string, string> data;
    }
    class AutorizationInfo
    {
        public static string token;
        public static string Start(string username, string password,string adres)
            ///summary
            /// введите имя пользвователя
        {
            try
            {
                var q = new user();
                q.username = username;
                q.password = password;
                string searilized = JsonConvert.SerializeObject(q);
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(adres);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Headers[HttpRequestHeader.Authorization] = "Bearer";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(searilized);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                var streamReader = new StreamReader(httpResponse.GetResponseStream());
                var result = JsonConvert.DeserializeObject<Auth>(streamReader.ReadToEnd());
                AutorizationInfo.token = "Bearer "+result.data["token"];
                return "ok";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}
