﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Stock
{
    /// <summary>
    /// Interaction logic for Autorization.xaml
    /// </summary>
    public partial class Autorization : Window
    {
        public Autorization()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            string result = AutorizationInfo.Start(textBoxLogin.Text, textBoxPassword.Text,RestAdres.Login);
            if (result == "ok")
            {
                var newWindows = new MainWindow();
                Hide();
                newWindows.ShowDialog();
                Show();                
            }
            else MessageBox.Show(result);
            
        }
    }
}
