﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Stock
{
    /// <summary>
    /// Interaction logic for add_client.xaml
    /// </summary>
    public partial class add_client : Window
    {
        private class newCl
        {
           public string name;
           public string lastname;
           public string phone;
           public string info;
           public string adres;
 
        }

        public add_client()
        {
            InitializeComponent();
        }


        private void button2_Click(object sender, RoutedEventArgs e)
        {   
            newCl cl = new newCl();
            cl.name= textBox.ToString() ;
            cl.lastname= textBox_Copy.ToString();
            cl.phone= textBox_Copy2.ToString();
            cl.info= textBox_Copy3.ToString();
            cl.adres= textBox_Copy4.ToString();
             
        }
    }
}
