﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Windows;

namespace Stock
{
   
    class RestService
    {
        static string token = AutorizationInfo.token;
        class ReturnData
        {
            public string statusCode;
            public string message;
            public List<Dictionary<string, string>> data;
        }
        /// <summary>
        /// Serverga jo'natish uchun ma'lumotlarni yuboring
        /// </summary>
        /// <param name="adres">Objectni serverda joylashgan adresi. Misol uchun - product yoki client.
        /// Barcha adreslar RestAdres ni ichida</param>
        /// <param name="data">Buni men urgatmasamham bilasizlar. 
        /// Har ehtimolga qarshi - bu jonatilayotgan ma'lumotimiz</param>
        /// <returns></returns>
        public static string POST(string adres, List<Type> data)
            {
                try
                {
                    string searilized = JsonConvert.SerializeObject(data);
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(adres);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Headers[HttpRequestHeader.Authorization] = token;
                    httpWebRequest.Method = "POST";
                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        streamWriter.Write(searilized);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    var streamReader = new StreamReader(httpResponse.GetResponseStream());            
                    return "ok";
                }
                catch (Exception ex)
                {
                    return ex.ToString();                
                }
            }
        public static object GET(string adres)
        {
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(adres);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Headers[HttpRequestHeader.Authorization] = token;
                httpWebRequest.Method = "GET";

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                var streamReader = new StreamReader(httpResponse.GetResponseStream());
                var result = JsonConvert.DeserializeObject<ReturnData>(streamReader.ReadToEnd());
                return result;
            }
            catch (Exception ex)
            {                
                return ex.ToString();
            }
        }
    }
}
